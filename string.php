<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>String PHP</title>
</head>
<body>
    <h1>Berlatih String PHP</h1>
    <?php   
        $kalimat1 = "PHP is never old";
        echo "kalimat pertama : " . $kalimat1 . "<br>";
        echo "panjang String : " . strlen($kalimat1) . "<br>";
        echo "Jumlah Kata : " . str_word_count($kalimat1);


        $first_sentence = "PHP is never old" ; // Panjang string 10, jumlah kata: 2
        $second_sentence = "I'm ready for the challenges"; // Panjang string: 28,  jumlah kata: 5
        
        echo "<h3> Soal No 2</h3>";
        $string2 = "I love PHP";
        echo "<label>String: </label> \"$string2\" <br>";
        echo "Kata pertama: " . substr($string2, 0, 1) . "<br>" ;
        echo "Kata Kedua: " . substr($string2, 2, 4) . "<br>" ; 
        echo "Kata Ketiga: " . substr($string2, 6, 7) . "<br>" ;

        echo "<h3> Soal No 3 </h3>";
        $string3 = "PHP is old but sexy!";
        echo "String : ". $string3. "<br>"; 
        echo "ganti kalimat ketiga : ". str_replace("sexy","awesome",$string3);
        
        // OUTPUT : "PHP is old but awesome"

    ?>
</body>
</html>